#!/bin/bash

## voxl-cross contains the following toolchains
## first two for apq8096, last for qrb5165
TOOLCHAIN_APQ8096_32="/opt/cross_toolchain/arm-gnueabi-4.9.toolchain.cmake"
TOOLCHAIN_APQ8096_64="/opt/cross_toolchain/aarch64-gnu-4.9.toolchain.cmake"
TOOLCHAIN_QRB5165="/opt/cross_toolchain/aarch64-gnu-7.toolchain.cmake"

# placeholder in case more cmake opts need to be added later
EXTRA_OPTS=""

## this list is just for tab-completion
AVAILABLE_PLATFORMS="qrb5165 apq8096"


print_usage(){
	echo ""
	echo " Build the current project based on platform target."
	echo ""
	echo " Usage:"
	echo ""
	echo "  ./build.sh apq8096"
	echo "        Build 64-bit binaries for apq8096"
	echo ""
	echo "  ./build.sh qrb5165"
	echo "        Build 64-bit binaries for qrb5165"
	echo ""
	echo ""
}

common () {
	echo "making common directories"
	install -d -m 755 misc_files/etc/systemd/system/
	install -d -m 755 misc_files/usr/bin/
	install -d -m 755 misc_files/usr/share/modalai/

	echo "copying common files"
	install -m 755 common/docker-autorun-script.sh misc_files/usr/share/modalai/

}

case "$1" in
	apq8096)
		common
		echo "Copying necessary files for APQ8096"
		install -m 755 apq8096/docker-daemon.service misc_files/etc/systemd/system/
		install -m 755 apq8096/docker-autorun.service misc_files/etc/systemd/system/
		install -m 755 apq8096/docker-prepare.sh misc_files/usr/bin/
		install -m 755 apq8096/voxl-configure-docker-support misc_files/usr/bin/
		install -m 755 apq8096/docker-hello-image.tgz misc_files/usr/share/modalai/
		;;
	qrb5165)
		common
		echo "Copying necessary files for QRB5165"
		install -m 755 qrb5165/docker-autorun.service misc_files/etc/systemd/system/
		install -m 755 qrb5165/docker-hello-image.tgz misc_files/usr/share/modalai/
		install -m 755 qrb5165/voxl-configure-docker-support misc_files/usr/bin/
		;;

	*)
		print_usage
		exit 1
		;;
esac

echo "done building"
