# voxl-docker-support

This package adds tools to improve the usability of docker on VOXL. It does not contain the docker package itself. similar to the matlab-support package in Debian.

apq8096 and qrb5165 have unique systemd service files and hello-world docker images. These are separated into respective folders, and the `common` folder contains shared files. build.sh puts the correct files into place for a paritcular platform, and make_package.sh bundles them up into a deb or ipk.
