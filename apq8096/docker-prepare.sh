#!/bin/bash

echo "preparing docker with docker-prepare.sh"
echo "this may take a few seconds"

if [ ! -f "/var/run/resolv.conf" ]; then
        /bin/echo "nameserver 8.8.8.8" > /var/run/resolv.conf
fi

set +e

mkdir -p /sys/fs/cgroup/cpuset/system.slice/

for i in {1..15}; do
    sleep 1

    # try to launch docker run to make cpuset.cpus appear
    # we expect this to fail! so set +e and redirect output to dev null
    /usr/bin/docker run --rm hello-world > /dev/null 2>&1

    if [ -f /sys/fs/cgroup/cpuset/system.slice/cpuset.cpus ]; then
        ## if the echo command works then then we are good!
        /bin/echo "0-3" > /sys/fs/cgroup/cpuset/system.slice/cpuset.cpus
        if [[ "$?" == "0"* ]]; then
            echo "docker-prepare SUCCESS after $i seconds"
            exit 0
        fi
    fi
done

echo "docker-prepare: failed to see cpuset appear after $i seconds"
exit 1
