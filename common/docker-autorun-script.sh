#!/bin/bash
## /etc/modalai/docker-autorun-script.sh

# This script is called by docker-autorun.service on boot (if enabled)
# Feel free to modify this file as you see fit to do whatever you want
# this is just a starting point
# Make sure these are run in non-interactive mode! e.g. use -n option with
# voxl-docker or dont use -it with docker run

## Hello World Example
echo "Launching hello-world docker image"
voxl-docker -n -i hello-image -e ""
echo "Done running hello-world docker image"

## mavros example
# voxl-docker -n -i roskinetic-xenial:v1.0 -w /root/yoctohome/mavros_test/ -e "/bin/bash run_mavros.sh"

